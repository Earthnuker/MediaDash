from datetime import datetime
from uuid import uuid4

from sqlalchemy import Float, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from api import Client

from . import db


class RequestItem(db.Model):
    id = db.Column(
        db.String,
        default=lambda: str(
            uuid4()),
        index=True,
        unique=True)
    item_id = db.Column(db.String, primary_key=True)
    added_date = db.Column(db.DateTime)
    request_type = db.Column(db.String)
    data = db.Column(db.String)
    approved = db.Column(db.Boolean, nullable=True)
    arr_id = db.Column(db.String, nullable=True)
    jellyfin_id = db.Column(db.String, nullable=True)
    users = relationship("RequestUser", back_populates="requests")

    @property
    def downloads(self):
        yield from self._download_state()

    @property
    def arr_item(self):
        c = Client()
        if self.request_type == "sonarr":
            return c.sonarr.series(self.arr_id)
        if self.request_type == "radarr":
            return c.radarr.movies(self.arr_id)

    def _download_state(self):
        c = Client()
        if self.request_type == "sonarr":
            q = c.sonarr.queue()
            for item in q:
                if item["seriesId"] == str(self.arr_id):
                    item["download"] = c.qbittorent.poll(self.download_id)
                    yield item
        c = Client()
        if self.request_type == "radarr":
            q = c.radarr.queue()
            for item in q:
                if str(item["movieId"]) == str(self.arr_id):
                    if item["protocol"] == "torrent":
                        item["download"] = c.qbittorent.poll(
                            item["downloadId"])
                    yield item


class RequestUser(db.Model):
    item_id = db.Column(
        db.String,
        db.ForeignKey(
            RequestItem.item_id),
        primary_key=True)
    user_id = db.Column(db.String, primary_key=True)
    hidden = db.Column(db.Boolean, default=False)
    updated = db.Column(db.Boolean, default=True)
    user_name = db.Column(db.String)
    requests = relationship("RequestItem", back_populates="users")

    @property
    def details(self):
        c = Client()
        return c.jellyfin.get_users(self.user_id)
