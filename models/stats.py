from datetime import datetime

from sqlalchemy import Column, DateTime, Float, Integer, String

from . import db


class Stats(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, default=datetime.today)
    key = db.Column(db.String)
    value = db.Column(db.Float)


class Diagrams(db.Model):
    name = db.Column(db.String, primary_key=True)
    data = db.Column(db.String)
