from datetime import datetime

from sqlalchemy import Column, DateTime, Float, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy_utils import JSONType

from . import db


class TranscodeJob(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime, default=datetime.today)
    status = db.Column(JSONType, default={})
    completed = db.Column(db.DateTime, default=None)
    profile = db.Column(db.String, default=None)
