import time
from urllib.parse import urljoin

import requests as RQ


class Jackett(object):
    def __init__(self, url, api_key):
        self.url = url
        self.api_key = api_key
        self.session = RQ.Session()
        self.session.post("http://192.168.2.25:9117/jackett/UI/Dashboard")

    def search(self, query, indexers=None):
        params = {"apikey": self.api_key,
                  "Query": query, "_": str(int(time.time()))}
        if indexers:
            params["Tracker[]"] = indexers
        res = self.session.get(
            urljoin(self.url, "api/v2.0/indexers/all/results"), params=params
        )
        res.raise_for_status()
        res = res.json()
        for val in res["Results"]:
            for prop in ["Gain", "Seeders", "Peers", "Grabs", "Files"]:
                val[prop] = val.get(prop) or 0
        return res

    def indexers(self):
        return [
            (t["id"], t["name"])
            for t in self.session.get(urljoin(self.url, "api/v2.0/indexers")).json()
            if t.get("configured")
        ]

    def test(self):
        errors = {}
        for idx, name in self.indexers():
            print("Testing indexer", name)
            result = self.session.post(
                urljoin(self.url, "api/v2.0/indexers/{}/test".format(idx))
            )
            if result.text:
                errors[name] = result.json()["error"]
        return errors
