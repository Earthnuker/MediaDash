from flask_login import UserMixin

from api import Jellyfin
from utils import handle_config


class JellyfinUser(UserMixin):
    def __init__(self, username, password):
        api = Jellyfin(handle_config()["jellyfin_url"], username, password)
        self.user = api.user
        self.api_key = api.api_key
        self.id = self.user["Id"]
        api.logout()

    def __getitem__(self, key):
        return self.user[key]

    @property
    def is_anonymous(self):
        return False

    @property
    def is_admin(self):
        pol = self.user["Policy"]
        return pol["IsAdministrator"]

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        pol = self.user["Policy"]
        return not pol["IsDisabled"]
