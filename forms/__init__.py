# -*- coding: utf-8 -*-
import json

from cryptography.hazmat.primitives.serialization import load_ssh_public_key
from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileField
from wtforms import (
    BooleanField,
    PasswordField,
    # RadioField,
    SelectField,
    SelectMultipleField,
    StringField,
    SubmitField,
    TextAreaField,
)
from wtforms.fields.html5 import SearchField
from wtforms.validators import URL, DataRequired, Optional
from wtforms.widgets import PasswordInput


def json_prettify(file):
    with open(file, "r") as fh:
        return json.dumps(json.load(fh), indent=4)


class RequestForm(FlaskForm):
    query = SearchField("Query", validators=[DataRequired()])
    search_type = SelectField(
        "Type", choices=[("sonarr", "TV Show"), ("radarr", "Movie")]
    )
    search = SubmitField("Search")


class SearchForm(FlaskForm):
    query = SearchField("Query", validators=[DataRequired()])
    tv_shows = BooleanField("TV Shows", default=True)
    movies = BooleanField("Movies", default=True)
    torrents = BooleanField("Torrents", default=True)
    indexer = SelectMultipleField(choices=[])
    group_by_tracker = BooleanField("Group torrents by tracker")
    search = SubmitField("Search")


class HiddenPassword(PasswordField):
    widget = PasswordInput(hide_value=False)


class TranscodeProfileForm(FlaskForm):
    test = TextAreaField()
    save = SubmitField("Save")


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = HiddenPassword("Password", validators=[DataRequired()])
    remember = BooleanField("Remember me")
    login = SubmitField("Login")


class AddSSHUser(FlaskForm):
    name = StringField("Name", validators=[DataRequired()])
    ssh_key = StringField("Public key", validators=[DataRequired()])
    add = SubmitField("Add")

    def validate_ssh_key(self, field):
        load_ssh_public_key(bytes(field.data, "utf8"))


class ConfigForm(FlaskForm):
    jellyfin_url = StringField("URL", validators=[URL()])
    jellyfin_api_key = StringField("API Key")

    qbt_url = StringField("URL", validators=[URL()])
    qbt_username = StringField("Username")
    qbt_passwd = HiddenPassword("Password")

    sonarr_url = StringField("URL", validators=[URL()])
    sonarr_api_key = HiddenPassword("API key")

    radarr_url = StringField("URL", validators=[URL()])
    radarr_api_key = HiddenPassword("API key")

    jackett_url = StringField("URL", validators=[URL()])
    jackett_api_key = HiddenPassword("API key")

    portainer_url = StringField("URL", validators=[URL()])
    portainer_username = StringField("Username")
    portainer_passwd = HiddenPassword("Password")

    transcode_default_profile = SelectField(
        "Default profile", choices=[], validators=[]
    )
    transcode_profiles = FileField(
        "Transcode profiles JSON",
        validators=[Optional(), FileAllowed(["json"], "JSON files only!")],
    )

    test = SubmitField("Test")
    save = SubmitField("Save")
