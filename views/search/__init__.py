from urllib.parse import unquote_plus

from flask import Blueprint, json, render_template, request

from api import Client
from forms import SearchForm
from utils import admin_required

search_page = Blueprint("search", __name__, url_prefix="/search")


@search_page.route("/details", methods=["GET", "POST"])
@admin_required
def details():
    data = {
        "info": json.loads(unquote_plus(request.form["data"])),
        "type": request.form["type"],
    }
    return render_template("search/details.html", **data)


@search_page.route("/", methods=["GET", "POST"])
@admin_required
def index():
    c = Client()
    results = {}
    params = request.args
    form = SearchForm()
    form.indexer.choices = c.jackett.indexers()
    if form.validate_on_submit():
        query = form.query.data
        if not (form.torrents.data or form.movies.data or form.tv_shows.data):
            form.torrents.data = True
            form.movies.data = True
            form.tv_shows.data = True

        if form.torrents.data:
            results["torrents"] = c.jackett.search(
                query, form.indexer.data or form.indexer.choices
            )
        if form.movies.data:
            results["movies"] = c.radarr.search(query)
        if form.tv_shows.data:
            results["tv_shows"] = c.sonarr.search(query)
        return render_template(
            "search/index.html",
            # form=form,
            search_term=query,
            results=results,
            client=c,
            group_by_tracker=form.group_by_tracker.data,
        )
    for name, field in form._fields.items():
        field.default = params.get(name)
    form.process()
    return render_template(
        "search/index.html",
        form=form,
        results={},
        group_by_tracker=False,
        sort_by="Gain",
    )
