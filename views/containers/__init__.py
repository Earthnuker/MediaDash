from flask import Blueprint, render_template

from api import Client
from utils import admin_required

containers_page = Blueprint("containers", __name__, url_prefix="/containers")


@containers_page.route("/")
@admin_required
def index():
    c = Client()
    containers = c.portainer.containers()
    return render_template("containers/index.html", containers=containers)


@containers_page.route("/<container_id>")
@admin_required
def details(container_id):
    c = Client()
    container = c.portainer.containers(container_id)
    return render_template("containers/details.html", container=container)
