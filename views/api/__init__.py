from flask import Blueprint, request, flash, session, redirect, url_for

import time
from api import Client
from utils import admin_required

api = Blueprint("api", __name__, url_prefix="/api")


@api.route("/add_torrent", methods=["POST"])
@admin_required
def add_torrent():
    category = request.form.get("category")
    c = Client()
    hashes_1 = set(c.qbittorent.status().get("torrents", {}))
    links = ""
    count = 0
    for link in request.form.getlist("torrent[]"):
        links += link + "\n"
        count += 1
    c.qbittorent.add(urls=links, category=category)
    for _ in range(10):
        status = c.qbittorent.status().get("torrents", {})
        hashes_2 = set(status)
        if len(hashes_2 - hashes_1) == count:
            break
        time.sleep(0.5)
    else:
        flash("Some torrents failed to get added to QBittorrent", "waring")
    new_torrents = sorted(hashes_2 - hashes_1)
    session["new_torrents"] = {h: status[h] for h in new_torrents}
    return redirect(url_for("search"))
