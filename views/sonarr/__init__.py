from flask import Blueprint, render_template

from api import Client
from utils import admin_required

sonarr_page = Blueprint("sonarr", __name__, url_prefix="/sonarr")


@sonarr_page.route("/")
@admin_required
def index():
    c = Client()
    series = c.sonarr.series()
    status = c.sonarr.status()
    history = c.sonarr.history()
    return render_template(
        "sonarr/index.html", series=series, status=status, history=history
    )


@sonarr_page.route("/<show_id>")
@admin_required
def details(show_id):
    return render_template("sonarr/details.html")
