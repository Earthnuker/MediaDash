from flask import Blueprint, render_template

from api import Client
from utils import admin_required

history_page = Blueprint("history", __name__, url_prefix="/history")


@history_page.route("/")
@admin_required
def index():
    c = Client()
    sonarr = c.sonarr.history()
    radarr = c.radarr.history()
    return render_template("history.html", sonarr=sonarr, radarr=radarr)
