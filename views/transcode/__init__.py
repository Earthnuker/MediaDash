from flask import Blueprint, render_template

from utils import admin_required
transcode_page = Blueprint("transcode", __name__, url_prefix="/transcode")


@transcode_page.route("/", methods=["GET", "POST"])
@admin_required
def index():
    return render_template("transcode/profiles.html")
