from flask import Blueprint, render_template
from flask_login import current_user

import stats_collect
from api import Client
from utils import login_required

jellyfin_page = Blueprint("jellyfin", __name__, url_prefix="/jellyfin")


@jellyfin_page.route("/")
@login_required
def index():
    c = Client()
    stats = stats_collect.Stats()
    jellyfin = {
        "info": c.jellyfin.system_info(),
        "status": c.jellyfin.status(),
        "counts": c.jellyfin.get_counts(),
        "library": stats["library"],
    }
    if not (current_user.is_authenticated and current_user.is_admin):
        jellyfin["library"] = {}
        jellyfin["status"]["HasUpdateAvailable"] = False
        jellyfin["status"]["HasPendingRestart"] = False

    return render_template("jellyfin/index.html", **jellyfin)


@jellyfin_page.route("/<item_id>")
@login_required
def details(item_id):
    c = Client()
    jellyfin = {
        "info": c.jellyfin.system_info(),
        "status": c.jellyfin.status(),
        "item": c.jellyfin.media_info(item_id),
    }
    if jellyfin["item"].get("Type") == "Movie":
        return render_template("jellyfin/movie.html", **jellyfin)
    if jellyfin["item"].get("Type") == "Series":
        return render_template("jellyfin/series.html", **jellyfin)
    return render_template("jellyfin/details.html", **jellyfin)
