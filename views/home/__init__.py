from flask import Blueprint, render_template
from flask_login import current_user

import stats_collect

home_page = Blueprint("home", __name__)


@home_page.route("/")
def index():
    stats = stats_collect.Stats()
    if not (current_user.is_authenticated and current_user.is_admin):
        stats["images"] = [
            img for img in stats["images"] if img[0] != "Torrents"]

    return render_template("index.html", fluid=True, data=stats)
