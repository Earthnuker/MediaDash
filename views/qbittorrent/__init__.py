from flask import Blueprint, render_template, request, redirect

from api import Client
from utils import admin_required

qbittorrent_page = Blueprint(
    "qbittorrent",
    __name__,
    url_prefix="/qbittorrent")


@qbittorrent_page.route("/")
@admin_required
def index():
    c = Client()
    qbt = c.qbittorent.status()
    sort_by_choices = {
        "speed": "Transfer Speed",
        "eta": "Time remaining",
        "state": "State",
        "category": "Category",
    }
    return render_template(
        "qbittorrent/index.html",
        qbt=qbt,
        status_map=c.qbittorent.status_map,
        state_filter=request.args.get("state"),
        sort_by=request.args.get("sort", "speed"),
        sort_by_choices=sort_by_choices,
    )


@qbittorrent_page.route("/add_trackers/<infohash>")
@admin_required
def add_trackers(infohash):
    c = Client()
    c.qbittorent.add_trackers(infohash)
    return redirect(url_for("qbittorrent_details", infohash=infohash))


@qbittorrent_page.route("/<infohash>")
@admin_required
def details(infohash):
    c = Client()
    qbt = c.qbittorent.status(infohash)
    return render_template(
        "qbittorrent/details.html", qbt=qbt, status_map=c.qbittorent.status_map
    )
