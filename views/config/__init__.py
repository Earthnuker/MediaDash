from flask import Blueprint, json, render_template, request
from api import Client
from forms import ConfigForm
from utils import admin_required, handle_config, populate_form, validate_transcoding_profiles

config_page = Blueprint("config", __name__, url_prefix="/config")


@config_page.route("/", methods=["GET", "POST"])
@admin_required
def index():
    form = ConfigForm()
    cfg = {}
    populate_form(form)
    if form.validate_on_submit():
        skip = ["save", "test", "csrf_token"]
        transcode_profiles = request.files.get("transcode_profiles")
        if transcode_profiles:
            try:
                form.transcode_profiles.data = json.load(transcode_profiles)
                validate_transcoding_profiles(form.transcode_profiles.data)
            except ValueError as e:
                form.transcode_profiles.data = None
                form.transcode_profiles.errors = [
                    "Invalid json data in file {}: {}".format(
                        transcode_profiles.filename, e
                    )
                ]
        else:
            form.transcode_profiles.data = handle_config().get("transcode_profiles", {})
        if form.errors:
            return render_template("config.html", form=form)
        for name, field in form._fields.items():
            if name in skip:
                continue
            cfg[name] = field.data
        if form.test.data:
            test_res = Client.test(cfg)
            populate_form(form, cfg)
            return render_template("config.html", form=form, test=test_res)
        handle_config(cfg)
        populate_form(form)
        return render_template("config.html", form=form)
    form.process()
    return render_template("config.html", form=form)
