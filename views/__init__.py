import sys

from flask import Blueprint

from .api import api  # noqa
from .config import config_page  # noqa
from .containers import containers_page  # noqa
from .history import history_page  # noqa
from .home import home_page  # noqa
from .jellyfin import jellyfin_page  # noqa
from .logs import logs_page  # noqa
from .qbittorrent import qbittorrent_page  # noqa
from .radarr import radarr_page  # noqa
from .remote import remote_page  # noqa
from .requests import requests_page  # noqa
from .search import search_page  # noqa
from .sonarr import sonarr_page  # noqa
from .transcode import transcode_page  # noqa


def register_blueprints(app):
    for k, v in vars(sys.modules[__name__]).items():
        if isinstance(v, Blueprint):
            app.register_blueprint(v)
