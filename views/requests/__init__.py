import base64
import json
from datetime import datetime

from flask import (
    Blueprint,
    current_app,
    flash,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from flask_login import current_user
from collections import defaultdict
from api import Client
from forms import RequestForm
from models import RequestItem, RequestUser
from utils import login_required, handle_config

requests_page = Blueprint("requests", __name__, url_prefix="/requests")


@requests_page.route("/<request_id>/<download_id>", methods=["GET"])
@login_required
def download_details(request_id, download_id):
    c = Client()
    request = RequestItem.query.filter(RequestItem.id == request_id).one_or_none()
    if request is None:
        flash("Unknown request ID", "danger")
        return redirect(url_for("requests.details", request_id=request_id))
    try:
        qbt = c.qbittorent.poll(download_id)
    except Exception:
        flash("Unknown download ID", "danger")
        return redirect(url_for("requests.details", request_id=request_id))
    return render_template("qbittorrent/details.html", qbt=qbt)


@requests_page.route("/<request_id>", methods=["GET"])
@login_required
def details(request_id):
    c = Client()
    if current_user.is_admin:
        requests = RequestItem.query
    else:
        user_id = current_user.get_id()
        requests = RequestItem.query.filter(
            RequestItem.users.any(RequestUser.user_id == user_id)
        )
    request = requests.filter(RequestItem.id == request_id).one_or_none()
    if request is None:
        flash("Unknown request ID", "danger")
        return redirect(url_for("requests.index"))
    RequestUser.query.filter(
        (RequestUser.user_id == current_user.id)
        & (RequestUser.item_id == request.item_id)
    ).update({RequestUser.updated: False})
    current_app.db.session.commit()
    jf_item = None
    arr = request.arr_item
    id_map = c.jellyfin.id_map()
    for key_id in ["tmdbId", "imdbId", "tvdbId"]:
        if key_id in arr:
            key = (key_id.lower()[:-2], str(arr[key_id]))
            jf_item = id_map.get(key, None)
            if jf_item:
                break
    return render_template(
        "requests/details.html", request=request, jellyfin_id=jf_item
    )


@requests_page.route("/", methods=["GET", "POST"])
@login_required
def index():
    c = Client()
    form = RequestForm()
    user_id = current_user.get_id()
    cfg = handle_config()
    used_requests = defaultdict(int)
    if current_user.is_admin:
        requests = RequestItem.query
    else:
        requests = RequestItem.query.filter(
            RequestItem.users.any(RequestUser.user_id == user_id)
        )
    for item in requests:
        if item.approved is None:
            used_requests[item.request_type] += 1
    remaining = {}
    remaining["movies"] = (
        cfg["num_requests_per_user"]["movies"] - used_requests["radarr"]
    )
    remaining["shows"] = cfg["num_requests_per_user"]["shows"] - used_requests["sonarr"]
    print("RQs:", used_requests, remaining)
    if (
        request.method == "POST"
        and ("approve" in request.form)
        or ("decline" in request.form)
    ):
        approved = "approve" in request.form
        declined = "decline" in request.form
        if approved and declined:
            flash("What the fuck?")
            approved = False
            declined = False
        if approved or declined:
            new_state = approved
            print("Approved:", approved)
        for item_id in request.form.getlist("selected[]"):
            item = RequestItem.query.get(item_id)
            if item.approved != new_state:
                RequestUser.query.filter(RequestUser.item_id == item_id).update(
                    {RequestUser.updated: True}
                )
            item.approved = new_state
            if new_state is True:
                search_type = item.request_type
                if hasattr(c, search_type):
                    api = getattr(c, search_type)
                    if hasattr(api, "add"):
                        result = api.add(json.loads(item.data))
                        print(result)
                        if item.request_type == "sonarr":
                            item.arr_id = result["seriesId"]
                        if item.request_type == "radarr":
                            item.arr_id = result["id"]
                    else:
                        flash("Invalid search type: {}".format(search_type), "danger")
            current_app.db.session.merge(item)
        current_app.db.session.commit()
        return render_template(
            "requests/index.html",
            results=[],
            form=form,
            search_type=None,
            requests=requests,
        )
        return redirect(url_for("requests.index"))
    if request.method == "POST" and form.search.data is False:
        request_type = request.form["search_type"]
        for item in request.form.getlist("selected[]"):
            data = str(base64.b64decode(item), "utf8")
            item = json.loads(data)
            item_id = "{type}/{titleSlug}/{year}".format(type=request_type, **item)
            user_id = session["jf_user"].get_id()
            request_entry = RequestItem.query.get(item_id)
            if request_entry is None:
                request_entry = RequestItem(
                    added_date=datetime.now(),
                    item_id=item_id,
                    users=[],
                    data=data,
                    request_type=request_type,
                )
                current_app.db.session.add(request_entry)
            request_entry.users.append(
                RequestUser(
                    user_id=user_id, item_id=item_id, user_name=current_user["Name"]
                )
            )
            current_app.db.session.merge(request_entry)
        current_app.db.session.commit()
        return render_template(
            "requests/index.html", results=[], form=form, requests=requests
        )
    if form and form.validate_on_submit():
        c = Client()
        query = form.query.data
        search_type = form.search_type.data
        if hasattr(c, search_type):
            api = getattr(c, search_type)
            if hasattr(api, "search"):
                results = api.search(query)
                return render_template(
                    "requests/index.html",
                    results=results,
                    form=form,
                    search_type=search_type,
                )
        flash("Invalid search type: {}".format(search_type), "danger")
    return render_template(
        "requests/index.html",
        results=[],
        form=form,
        search_type=None,
        requests=requests,
    )


"""
    if form.validate_on_submit():
        query = form.query.data
        if not (form.torrents.data or form.movies.data or form.tv_shows.data):
            form.torrents.data = True
            form.movies.data = True
            form.tv_shows.data = True

        if form.torrents.data:
            results["torrents"] = c.jackett.search(
                query, form.indexer.data or form.indexer.choices
            )
        if form.movies.data:
            results["movies"] = c.radarr.search(query)
        if form.tv_shows.data:
            results["tv_shows"] = c.sonarr.search(query)
        return render_template(
            "search/index.html",
            # form=form,
            search_term=query,
            results=results,
            client=c,
            group_by_tracker=form.group_by_tracker.data,
        )
"""
