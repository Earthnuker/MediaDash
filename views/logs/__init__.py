from flask import Blueprint, render_template

from api import Client
from utils import admin_required

logs_page = Blueprint("log", __name__, url_prefix="/log")


@logs_page.route("/")
@admin_required
def index():
    c = Client()
    logs = {
        "radarr": c.radarr.log(),
        "sonarr": c.sonarr.log(),
        "qbt": c.qbittorent.log(),
        "peers": c.qbittorent.peer_log(),
    }
    return render_template("logs.html", logs=logs)
