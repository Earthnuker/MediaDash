from flask import Blueprint, render_template

from api import Client
from utils import admin_required

radarr_page = Blueprint("radarr", __name__, url_prefix="/radarr")


@radarr_page.route("/")
@admin_required
def index():
    c = Client()
    movies = c.radarr.movies()
    status = c.radarr.status()
    history = c.radarr.history()
    return render_template(
        "radarr/index.html", movies=movies, status=status, history=history
    )


@radarr_page.route("/<movie_id>")
@admin_required
def details(movie_id):
    return render_template("radarr/details.html")
